function race_corrs = MeasureCorr()
race_corrs = [];
for i = 17:26
    table = importdata([num2str(i) '.txt']);
    allcorrs = corr(table,'type','Kendall','rows','pairwise');
    race_corrs = [race_corrs mean(allcorrs(~diag(true(size(allcorrs,1),1))))];
end

end