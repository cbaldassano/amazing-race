function [elim_winners, firstplace_winners, rand_pick, order_corr] = Race()
rng(1);
N = 11;
num_shuff = 10000;
noise_levels = [0:0.5:2 2.1:.1:2.5 3:0.5:10];

elim_winners = zeros(num_shuff,length(noise_levels));
firstplace_winners = zeros(num_shuff, length(noise_levels));
rand_pick = zeros(num_shuff,length(noise_levels));
order_corr = zeros(num_shuff,length(noise_levels));
for n = 1:length(noise_levels)
    disp([num2str(n) '/' num2str(length(noise_levels))]);
    for s = 1:num_shuff
        teams = sort(randn(N,1),'descend');
        legvals = repmat(teams,1,9) + randn(N,9)*noise_levels(n);
        [~,ordering] = sort(legvals,'descend');
        allcorrs = corr(ordering,'type','Kendall');
        order_corr(s,n) = mean(allcorrs(~diag(true(9,1))));
        
        elim_winners(s,n) = ElimLast(legvals);
        firstplace_winners(s,n) = CountWins(legvals);
        rand_pick(s,n) = randi(N,1,1);
    end
end

end

function winner = ElimLast(legvals)
N = size(legvals,1);
remaining_teams = true(N,1);
for i = 1:8
    legval = legvals(:,i);
    legval(~remaining_teams) = Inf;
    [~,lastteam] = min(legval);
    remaining_teams(lastteam) = false;
end

lastleg = legvals(:,9);
lastleg(~remaining_teams) = -Inf;
[~, winner] = max(lastleg);
end

function winner = CountWins(legvals)
N = size(legvals,1);
leg_winners = zeros(9,1);
for i = 1:9
    legval = legvals(:,i);
    [~,leg_winners(i)] = max(legval);
end

score_table = tabulate(leg_winners);
maxscore = max(score_table(:,2));
tiedwinners = score_table(score_table(:,2)==maxscore, 1);
winningleg = find(ismember(leg_winners,tiedwinners),1,'last');
winner = leg_winners(winningleg);
end